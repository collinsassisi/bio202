L = 4;
x = 0:0.05:L;
N = 5;
n = 1:2:N;
Bn = 4./(n*pi);
D = 2;K = 1; 

for t = 0:0.1:100
    S = 0*rand(size(x));
    for ii = 1:length(n)
        S = S + Bn(ii)*sin((n(ii)*pi*x)./L)*exp((K - n(ii)^2*pi^2*D/L^2)*t);
    end
    plot(x,S); axis([0 L 0 1]); title(['L = ',num2str(L),' Lc = ', num2str(pi*sqrt(D/K))])
    drawnow
end

