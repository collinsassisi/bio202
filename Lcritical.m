function Lc = Lcritical(D,K)
%function to calculate the critical length
%in a linear 1-D reaction diffusion equation

Lc = pi*sqrt(D/K);


end

