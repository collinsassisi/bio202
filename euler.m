function [T,Y] = euler(f,a,b,ya,m,p)
%---------------------------------------------------------------------------
%   Euler method to solve ODEs of the form y' = f(t,y) with y(a) = ya.
% Sample call
%   [T,Y] = euler('f',a,b,ya,m,p)
% Inputs
%   f    name of the function
%   a    left  endpoint of [a,b]
%   b    right endpoint of [a,b]
%   ya   initial value
%   m    number of steps
%   p    function parameters
% Return
%   T    solution: vector of abscissas
%   Y    solution: vector of ordinates
%
%
%---------------------------------------------------------------------------

h = (b - a)/m;
T = zeros(1,m+1);
Y = zeros(length(ya),m+1);
T(1) = a;
Y(:,1) = ya;
for j=1:m,
  tj = T(j);
  yj = Y(:,j);
  Y(:,j+1) = yj + h*feval(f,tj,yj,p);
  T(j+1) = a + h*j;
end