function udot = ExponentialDecay(t,u,k)

%RHS of the ODE -------------------------
%dx/dt = kx
% Sample call
% udot = Exponential(t,u,k)
% t --> time
% u --> initial value
% k --> growth rate
%-----------------------------------------

udot = k*u;